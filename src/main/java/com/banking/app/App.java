package com.banking.app;

import com.banking.app.models.CreditorDebtorRecord;
import com.banking.app.models.User;
import com.banking.app.services.WalletService;

import java.util.Scanner;

public class App {

    private static final String LOGIN_COMMAND = "login";
    private static final String PAY_COMMAND = "pay";
    private static final String TOP_UP_COMMAND = "topup";
    private static final String SHOW_ALL = "showall";

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in);
        WalletService walletService = new WalletService();

        try {
            while (true) {
                UserManager userManager = UserManager.getInstance();
                String commandInput = scanner.nextLine();
                String[] commandArray = commandInput.split(" ");

                if (commandArray.length >= 1) {
                    String command = commandArray[0];
                    String input2 = commandArray.length > 1 ? commandArray[1] : "";
                    String input3 = commandArray.length > 2 ? commandArray[2] : "";

                    switch (command) {
                        case LOGIN_COMMAND:
                            String username = input2;
                            userManager.login(username);

                            if (userManager.getLoggedInUser() == null) {
                                System.out.println("Invalid user, please try again!");
                                break;
                            }

                            System.out.println("Hello," + username + "!");
                            walletService.checkDebts();
                            printCurrentLoggedInUserBalance();
                            break;
                        case PAY_COMMAND:
                            if(!userManager.isLoggedIn()) {
                                System.out.println("Please login!");
                                break;
                            }

                            if(!walletService.isSufficientFund()) {
                                System.out.println("Insufficient balance, please top up!");
                                break;
                            }

                            User receiver = userManager.getUserByUsername(input2);
                            int payAmount = Integer.parseInt(input3);
                            walletService.payTo(receiver, payAmount);
                            printCurrentLoggedInUserBalance();
                            printDebts(walletService);
                            break;
                        case TOP_UP_COMMAND:
                            if(!userManager.isLoggedIn()) {
                                System.out.println("Please login!");
                                break;
                            }
                            int topUpAmount = Integer.parseInt(input2);
                            walletService.topUp(topUpAmount);
                            printCurrentLoggedInUserBalance();
                            break;
                        case SHOW_ALL:
                            for(User user: UserManager.getInstance().getUsers()) {
                                System.out.println(user.toString());
                            }
                            break;
                        default:
                            System.out.println("Invalid Command");
                    }
                }
            }
        } catch(IllegalStateException e) {
            // System.in has been closed
            System.out.println("System.in was closed; exiting");
        }
    }

    private static void printCurrentLoggedInUserBalance() {
        System.out.println("Your balance is " + UserManager.getInstance().getLoggedInUser().getBalance());
    }

    private static void printDebts(WalletService walletService) {
        if(UserManager.getInstance().isLoggedIn()) {
            CreditorDebtorRecord debtorRecord = walletService.getDebtRecord();

            if(debtorRecord != null) {
                System.out.println("Owing " + debtorRecord.getAmount() + " to " + debtorRecord.getCreditor().getUsername());
            }
        }
    }
}
