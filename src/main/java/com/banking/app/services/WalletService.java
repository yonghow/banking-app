package com.banking.app.services;

import com.banking.app.UserManager;
import com.banking.app.models.CreditorDebtorRecord;
import com.banking.app.models.User;

import java.util.ArrayList;

public class WalletService {
    private final ArrayList<CreditorDebtorRecord> creditorDebtorRecords;
    private final UserManager userManager;

    public WalletService() {
        creditorDebtorRecords = new ArrayList<>();
        userManager = UserManager.getInstance();
    }

    public void payTo(User receiver, int payAmount) {
        User loggedInUser = userManager.getLoggedInUser();
        int payerNewBalance = loggedInUser.getBalance() - payAmount;

        // deduct from receiver debt if available
        CreditorDebtorRecord creditorRecord = getCreditorRecord();
        if(creditorRecord != null) {
            if(creditorRecord.getAmount() > payAmount) {
                int newCreditorAmount = creditorRecord.getAmount() - payAmount;
                creditorRecord.setAmount(newCreditorAmount);
                return;
            }
        }

        if(payerNewBalance > 0) {
            // enough fund to pay
            loggedInUser.setBalance(payerNewBalance);
            int debtRepayBalanceAmount = 0;

            CreditorDebtorRecord debtorRecord = getDebtRecord();
            if(debtorRecord != null) {
                if(payAmount >= debtorRecord.getAmount()) {
                    debtRepayBalanceAmount = payAmount - debtorRecord.getAmount();
                    debtorRecord.setAmount(0);
                } else {
                    debtorRecord.setAmount(debtorRecord.getAmount() - payAmount);
                }
            }

            if(debtRepayBalanceAmount > 0) {
                receiver.setBalance(receiver.getBalance() + debtRepayBalanceAmount);
            } else {
                receiver.setBalance(receiver.getBalance() + payAmount);
            }

            System.out.println("Transferred " + payAmount + " to " + receiver.getUsername());
        } else {
            // not enough fund to pay the owe receiver
            int amountOwe = payAmount - loggedInUser.getBalance();
            int amountPartialPayment = loggedInUser.getBalance();
            receiver.setBalance(receiver.getBalance() + amountPartialPayment);
            loggedInUser.setBalance(0);

            CreditorDebtorRecord creditorDebtorRecord = new CreditorDebtorRecord(receiver, loggedInUser, amountOwe);
            creditorDebtorRecords.add(creditorDebtorRecord);

            System.out.println("Transferred " + amountPartialPayment + " to " + receiver.getUsername());
        }
    }

    public void topUp(int topUpAmount) {
        User loggedInUser = userManager.getLoggedInUser();

        CreditorDebtorRecord debtorRecord = creditorDebtorRecords
                .stream()
                .filter(debtor -> debtor.getDebtor().getUsername().equalsIgnoreCase(loggedInUser.getUsername()))
                .findAny()
                .orElse(null);

        if(debtorRecord != null) {
            int tempTotalBalance = topUpAmount + loggedInUser.getBalance();
            int debtorFinalBalance = tempTotalBalance - debtorRecord.getAmount();

            if(debtorFinalBalance < 0) {
                loggedInUser.setBalance(0);
                int balanceOwe = debtorRecord.getAmount() - topUpAmount;
                debtorRecord.getCreditor().setBalance(debtorRecord.getCreditor().getBalance() + topUpAmount);
                debtorRecord.setAmount(balanceOwe);
            } else {
                loggedInUser.setBalance(debtorFinalBalance);
                debtorRecord.getCreditor().setBalance(debtorRecord.getCreditor().getBalance() + debtorRecord.getAmount());
            }
        } else {
            int newBalance = loggedInUser.getBalance() + topUpAmount;
            loggedInUser.setBalance(newBalance);
        }
    }

    public void checkDebts() {
        User loggedInUser = userManager.getLoggedInUser();

        for(CreditorDebtorRecord creditorDebtorRecord : creditorDebtorRecords) {
            if(creditorDebtorRecord.getCreditor().getUsername().equalsIgnoreCase(loggedInUser.getUsername())) {
                System.out.println("Owing " + creditorDebtorRecord.getAmount()  + " from " + creditorDebtorRecord.getDebtor().getUsername() + ".");
                return;
            }

            if(creditorDebtorRecord.getDebtor().getUsername().equalsIgnoreCase(loggedInUser.getUsername())) {
                System.out.println("Owing " + creditorDebtorRecord.getAmount()  + " to " + creditorDebtorRecord.getCreditor().getUsername() + ".");
            }
        }
    }

    public boolean isSufficientFund() {
        return userManager.getLoggedInUser().getBalance() != 0;
    }

    public CreditorDebtorRecord getDebtRecord() {
        if(!UserManager.getInstance().isLoggedIn()) {
            return null;
        }

        String loggedInUsername = userManager.getLoggedInUser().getUsername();
        return creditorDebtorRecords
                .stream()
                .filter(debtor -> debtor.getDebtor().getUsername().equalsIgnoreCase(loggedInUsername))
                .findAny()
                .orElse(null);
    }

    public CreditorDebtorRecord getCreditorRecord() {
        if(!UserManager.getInstance().isLoggedIn()) {
            return null;
        }

        String loggedInUsername = userManager.getLoggedInUser().getUsername();
        return creditorDebtorRecords
                .stream()
                .filter(creditor -> creditor.getCreditor().getUsername().equalsIgnoreCase(loggedInUsername))
                .findAny()
                .orElse(null);
    }
}
