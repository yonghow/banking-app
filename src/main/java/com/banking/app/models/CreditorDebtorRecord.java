package com.banking.app.models;

public class CreditorDebtorRecord {
    private User creditor;
    private User debtor;
    private int amount;

    public CreditorDebtorRecord(User creditor, User debtor, int amount) {
        this.creditor = creditor;
        this.debtor = debtor;
        this.amount = amount;
    }

    public User getCreditor() {
        return creditor;
    }

    public void setCreditor(User creditor) {
        this.creditor = creditor;
    }

    public User getDebtor() {
        return debtor;
    }

    public void setDebtor(User debtor) {
        this.debtor = debtor;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }
}
