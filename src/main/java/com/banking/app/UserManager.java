package com.banking.app;

import com.banking.app.models.User;

import java.util.ArrayList;
import java.util.Arrays;

public class UserManager {

    private static UserManager userManager;
    private static ArrayList<User> users;
    private static User loggedInUser;

    public UserManager() {
        User alice = new User("Alice", 0);
        User bob = new User("Bob", 0);
        users = new ArrayList<>(Arrays.asList(alice, bob));
        loggedInUser = null;
    }

    public void login(String username) {
        loggedInUser = getInstance().getUserByUsername(username);
    }

    public static UserManager getInstance() {
        if(userManager == null) {
            userManager = new UserManager();
        }

        return userManager;
    }

    public User getLoggedInUser() {
        return loggedInUser;
    }

    public User getUserByUsername(String username) {
        return users
                .stream()
                .filter(u -> u.getUsername().equalsIgnoreCase(username))
                .findAny()
                .orElse(null);
    }

    public ArrayList<User> getUsers() {
        return users;
    }

    public boolean isLoggedIn() {
        return loggedInUser != null;
    }
}
