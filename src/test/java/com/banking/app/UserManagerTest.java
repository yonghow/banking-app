package com.banking.app;

import com.banking.app.models.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class UserManagerTest {

    private static UserManager userManager;

    @BeforeAll
    public static void setup(){
        userManager = UserManager.getInstance();
    }

    @Test
    @DisplayName("Test login")
    public void testLogin() {
        userManager.login("Alice");
        assertEquals("Alice", userManager.getLoggedInUser().getUsername(), "Test login success");
    }

    @Test
    @DisplayName("Test invalid login")
    public void testInvalidLogin() {
        userManager.login("leslie");
        assertNull(userManager.getLoggedInUser(), "Test invalid login");
    }

    @Test
    @DisplayName("Test get user by username")
    public void testGetUserByUsername() {
        User aliceUser = userManager.getUserByUsername("Alice");
        assertEquals("Alice", aliceUser.getUsername(), "Test get user by username");
    }

    @Test
    @DisplayName("Test get logged in user")
    public void testGetLoggedInUser() {
        UserManager.getInstance().login("Alice");
        assertEquals("Alice", userManager.getLoggedInUser().getUsername(), "Test get logged in user");
    }
}
