package com.banking.app.services;

import com.banking.app.UserManager;
import com.banking.app.models.CreditorDebtorRecord;
import com.banking.app.models.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WalletServiceTest {

    private static WalletService walletService;
    private static UserManager userManager;

    @BeforeEach
    public void beforeEach(){
        userManager = UserManager.getInstance();
        walletService = new WalletService();
        userManager.getUserByUsername("alice").setBalance(0);
        userManager.getUserByUsername("bob").setBalance(0);
    }

    @Test
    @DisplayName("Test pay to user")
    public void testPayToUser() {
        User bobUser = UserManager.getInstance().getUserByUsername("bob");
        userManager.login("alice");
        walletService.topUp(100);
        walletService.payTo(bobUser, 50);

        assertEquals(50, bobUser.getBalance(), "Bob received fund successfully");
        assertEquals(50, userManager.getLoggedInUser().getBalance(), "Alice balance deducted successfully");
    }

    @Test
    @DisplayName("Test pay to user when and owning")
    public void testPayToUserWithOwning() {
        User bobUser = userManager.getUserByUsername("bob");
        userManager.login("alice");
        walletService.topUp(100);
        walletService.payTo(bobUser, 200);
        CreditorDebtorRecord debtRecord = walletService.getDebtRecord();

        assertEquals(100, bobUser.getBalance(), "Bob received partial fund");
        assertEquals(0, userManager.getLoggedInUser().getBalance(), "Alice balance goes to 0");
        assertEquals(100, debtRecord.getAmount(), "Owning amount 100");
        assertEquals(bobUser, debtRecord.getCreditor(), "Owning person is bob");
    }

    @Test
    @DisplayName("Test top up")
    public void testTopUp() {
        userManager.login("alice");
        walletService.topUp(50);

        assertEquals(50, userManager.getLoggedInUser().getBalance(), "Alice top up successfully");
        assertEquals(0, userManager.getUserByUsername("bob").getBalance(), "Bob balance did not touch");
    }

    @Test
    @DisplayName("Test top up with partial repayment")
    public void testTopUpWithPartialRepayment() {
        User bobUser = userManager.getUserByUsername("bob");
        userManager.login("alice");
        walletService.topUp(100);
        walletService.payTo(bobUser, 200);

        assertEquals(0, userManager.getLoggedInUser().getBalance(), "Alice balance go to 0");
        assertEquals(100, bobUser.getBalance(), "Bob received partial fund");

        walletService.topUp(50);

        assertEquals(0, userManager.getLoggedInUser().getBalance(), "Alice balance still 0 because repay to bob");
        assertEquals(150, bobUser.getBalance(), "Bob balance increased 50 by partial repayment");
    }

    @Test
    @DisplayName("Test top up with full repayment")
    public void testTopUpWithFullRepayment() {
        User bobUser = userManager.getUserByUsername("bob");
        userManager.login("alice");
        walletService.topUp(100);
        walletService.payTo(bobUser, 200);

        assertEquals(0, userManager.getLoggedInUser().getBalance(), "Alice balance go to 0");
        assertEquals(100, bobUser.getBalance(), "Bob received partial fund");

        walletService.topUp(200);

        assertEquals(100, userManager.getLoggedInUser().getBalance(), "Alice did full repayment and top up successfully");
        assertEquals(200, bobUser.getBalance(), "Bob received full repayment from alice");
    }

    @Test
    @DisplayName("Test coding challenge")
    public void testCodingChallenge() {
        User aliceUser = userManager.getUserByUsername("Alice");
        User bobUser = userManager.getUserByUsername("Bob");

        userManager.login("Alice");
        assertEquals("Alice", userManager.getLoggedInUser().getUsername(), "Alice logged in");

        walletService.topUp(100);
        assertEquals(100, aliceUser.getBalance(), "Alice top up 100 successfully");

        userManager.login("Bob");
        assertEquals("Bob", userManager.getLoggedInUser().getUsername(), "Bob logged in");

        walletService.topUp(80);
        assertEquals(80, bobUser.getBalance(), "Bob top up 80 successfully");

        walletService.payTo(aliceUser, 50);
        assertEquals(150, aliceUser.getBalance(), "Alice received 50 from Bob");
        assertEquals(30, bobUser.getBalance(), "Bob balance left 30 after transfer 50 to Alice");

        walletService.payTo(aliceUser, 100);
        assertEquals(180, aliceUser.getBalance(), "Alice received partial payment 30 from Bob");
        assertEquals(0, bobUser.getBalance(), "Bob balance is 0");
        assertEquals(aliceUser, walletService.getDebtRecord().getCreditor(), "Bob owe Alice money");
        assertEquals(70, walletService.getDebtRecord().getAmount(), "Bob owe Alice 70");

        walletService.topUp(30);
        assertEquals(210, aliceUser.getBalance(), "Alice received partial payment 30 from Bob");
        assertEquals(0, bobUser.getBalance(), "Bob balance is 0");
        assertEquals(aliceUser, walletService.getDebtRecord().getCreditor(), "Bob owe Alice money");
        assertEquals(40, walletService.getDebtRecord().getAmount(), "Bob owe Alice 40");

        userManager.login("Alice");
        assertEquals("Alice", userManager.getLoggedInUser().getUsername(), "Alice logged in");

        walletService.payTo(bobUser, 30);
        assertEquals(210, aliceUser.getBalance(), "Alice pay Bob but deduct from Bob's debt");
        assertEquals(0, bobUser.getBalance(), "Bob balance should be 0");
        assertEquals(10, walletService.getCreditorRecord().getAmount(), "Bob's debt updated from Alice payment");

        userManager.login("Bob");
        assertEquals("Bob", userManager.getLoggedInUser().getUsername(), "Bob logged in");

        walletService.topUp(100);
        assertEquals(220, aliceUser.getBalance(), "Alice should receive repayment 10 from bob");
        assertEquals(90, bobUser.getBalance(), "Bob balance should be 90");
    }
}
