# Code Challenge

## Prerequisite
1. Java 11
1. Maven

## How to run
Use any IDE with Maven support such as eclipse or InteliJ can run as normal Java Application.

## Command
1. `login <username>`
1. `topup <amount>` - only after logged in.
1. `pay <receiver> <amount>` - only after logged in.